'use strict'
const router = require('express').Router();
const db = require('../db');
var nodemailer = require('nodemailer');

// create reusable transporter object using the default SMTP transport
let sendMail = ()=>{
    var transporter = nodemailer.createTransport('smtps://harshit.gohil10@gmail.com:pass@smtp.gmail.com');

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: '"Harshit" harshit.gohil10@gmail.com', // sender address
        to: 'harshit@discoverdollar.com', // list of receivers
        subject: 'Hello ✔', // Subject line
        text: 'Hello world ?', // plaintext body
        html: '<b>Hello world ?</b>' // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            return console.log(error);
        }
        console.log('Message sent: ' + info.response);
    });
}

let regrRoute = (r, m) => {
    for (let k in r) {
        ////console.log(k);
        ////console.log(typeof r[k] === 'object' && r[k] !== null && !(r[k] instanceof Array));
        if (typeof r[k] === 'object' & r[k] !== null & !(r[k] instanceof Array)) {
            ////console.log(r[k]," instanceof Array " + (r[k] instanceof Array));
            regrRoute(r[k], k);
        } else {
            if (m == 'GET') {
                router.get(k, r[k]);
                ////console.log( "method is " + m,'route function is ' + r[k]);
            } else if (m == 'POST') {
                router.post(k, r[k]);
                ////console.log( m,r[k]);
            } else {
                router.use(r[k]);
            }
        }
    }
}

let route = routes => {
    regrRoute(routes);
    return router
}
let getIdeasByID = (id) => {
    console.log("data recived is ",id);
    return db.ideaModel.findOne({'_id':id});
}
let getIdeas = () => {
    return db.ideaModel.find();
}
let instIdea = data => {
    return db.Mongoose.connection.collection('ideas').insert(data);
}
let authenticate = req => {
    //console.log(JSON.stringify({ "uid": req.body.uid, "pswd": req.body.pswd }));
    return db.userModel.findOne({ "uid": req.body.uid, "pswd": req.body.pswd });
}
let setDomains = (req, data) => {
    let allDomain = req.session.allDomain || [];
    data.forEach(row => {
        let flag = false;
        for (var i = 0, len = allDomain.length; i < len; i++) {
            if (allDomain[i].name == row.domain) {
                flag = true;
                break;
            }
        }
        if (flag == false) {
            allDomain.push({
                name: row.domain,
                sd: []
            });
        }
    });
    return allDomain;
}
let setdm = (allDomain)=>{
    return new Promise( (res,rej)=>{
        let i=0;
        for ( i = 0; i < allDomain.length; i++) {
            let dom = allDomain[i]; 
//            //console.log("Sub Domains Set",allDomain[i]);
            sdm(dom)
            .then((sdn) => {
                dom.sd = sdn;
                //console.log("Sub Domain in all Domain ", dom);
                if(allDomain[0].sd[0] !== undefined){
                    //console.log("Sub Domain in all Domain ", allDomain[0].sd[0]);
                    res(allDomain);
                }
            });
        }
        //console.log("Sub Domain in all Domain ", allDomain[0].sd[0]);
        
    });
}
let sdm = (data) => {
    //console.log("Sub Domains Method Called...");
    return db.ideaModel.distinct('subDomain', { "domain": data.name });
}

let allidea = [];
let getallidea = ()=>{
    refreshAllIdea();
    return allidea;
}
let getSpidea = (qry)=>{
    console.log("subDomain" + qry['sd'],"domain",qry['dom']);
    db.ideaModel.find({"subDomain":qry['sd'],"domain":qry['dom']})
    .then(data=>{
        allidea = data;
        console.log("all idea === ", allidea);
    })
    .catch(err=>{console.log("err",err);});
    return allidea;
}
let refreshAllIdea = ()=>{
    db.ideaModel.find()
    .then(data=>{
        allidea = data;
    })
    .catch(err=>{console.log("err",err);});
}
refreshAllIdea();
let getUsers = ()=>{
    return db.userModel.find({},{"pswd":0});
}
module.exports = {
    route,
    authenticate,
    getIdeas,
    instIdea,
    getUsers,
    setDomains,
    getallidea,
    getSpidea,
    setdm,
    sendMail,
    getIdeasByID
}