'use strict';
const h = require('../helper');
const logger = require('../logger');
module.exports = (io,app) =>{
    let allDomains = app.locals.domains;
    io.of('/users').on('connection',socket=>{
        socket.on('getusers',()=>{
            socket.emit('userLst',JSON.stringify(h.getUsers()));
        });
    });

    io.of('/ideaPage').on('connection', socket => {
    socket.on('join', data => {
        console.log("New User joined to ideaPage");
        //socket.broadcast.to(data.roomID).emit('updateIdeaList', JSON.stringify(usersList.users));
        //socket.emit('updateIdeaList', JSON.stringify(idea));
    });

/*    socket.on('disconnect', () => {
        let room = h.removeUserFromRoom(allrooms, socket);
        socket.broadcast.to(room.roomID).emit('updateUsersList', JSON.stringify(room.users));
    });
*/
    socket.on('newMessage', data => {
        socket.to(data.roomID).emit('inMessage', JSON.stringify(data));
    });
    });
    
}