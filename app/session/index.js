'use strict';
const session = require('express-session');
const logger = require('../logger');
const MongoStore = require('connect-mongo')(session);
const config = require('../config');
const db = require('../db');

if(process.env.NODE_ENV=== 'production'){
    // Initialize session  with setting  for Production 
    module.exports=session({
        secret : config.secret,
        resave : false,
        saveUninitialized : false,
        store : new MongoStore({
            mongooseConnection :db.Mongoose.Connection
        })
    });
}else{
    // Initialize session  with setting  for dev
    module.exports=session({
        secret : config.secret,
        resave : false,
        saveUninitialized : false,
    });
}