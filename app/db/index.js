'use strict';
const config = require('../config');
const logger = require('../logger');
const Mongoose = require('mongoose').connect(config.dbURI);

Mongoose.connection.on('error',err=>{
    logger.log("0000000000000000000000000000000000000000000000000000000");
    logger.error(err);
    logger.log("0000000000000000000000000000000000000000000000000000000");
});
//Creating Schema
const users = new Mongoose.Schema({
    _id:String,
    uid:String,
    pswd:String
});
const idea = new Mongoose.Schema({
    _id:{type : String, index:{ unique: true }},
    uid:String,
    domain:String,
    subDomain:String,
    tittle:String,
    ibody:String,
    status:String,
    keywords: [],
    dateTime:Date,
    Comments:[{cuid:String, date:Date,body:String}]
});
const sd = new Mongoose.Schema({
    sdName:String,
    keywords:[String]
});
const domain = new Mongoose.Schema({
    _id:String,
    domain:String,
    subDomain:[sd]
});
//Turn the Schema into a Collection in DB
let userModel = Mongoose.model('users',users);
let ideaModel = Mongoose.model('ideas',idea);
let domainModel = Mongoose.model('domains',domain);
module.exports = {
    Mongoose,
    userModel,
    ideaModel,
    domainModel
}