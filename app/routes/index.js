'use strict';
const h = require('../helper');
const passport = require('passport');
const logger = require('../logger');
const config = require('../config');
module.exports = () => {
    let routes = {
            //all get methods;
            'GET': {
                '/home': (req, res, next) => {
                    res.render('home');
                },
                '/flot': (req, res, next) => {
                    res.render('flot');
                },
                '/buttons': (req, res, next) => {
                    res.render('buttons');
                },
                '/search': (req, res, next) => {
                    console.log("/search called and parameters are ",req.query);
                    let allideas = [];
                    if(req.query["sd"] == undefined){
                        console.log("/search called anreq.query");
                        allideas = h.getallidea();
                    }else
                    {
                        console.log('"/search called anreq.queryconsole.log("/search called anreq.query");"');
                        allideas = h.getSpidea(req.query);
                    }
                    if (req.session._id == undefined){
                        res.render('login');
                    }
                    else if (allideas[0] !== undefined) {
                        req.session.AnyIdea = true;
                        res.render('searchTab', {
                            uid: req.session.uid,
                            _id: req.session._id,
                            userLst:req.session.allUser,
                            host: config.host,
                            iData: allideas
                        });
                    }
                    else
                        res.redirect('/timeline');
                },
                '/forms': (req, res, next) => {
                    res.render('forms');
                },
                '/grid': (req, res, next) => {
                    res.render('grid');
                },
                '/icons': (req, res, next) => {
                    res.render('icons');
                },
                '/morris': (req, res, next) => {
                    res.render('morris');
                },
                '/panels-wells': (req, res, next) => {
                    res.render('panels-wells');
                },
                '/tables': (req, res, next) => {
                    res.render('tables');
                },
                '/timeline': (req, res, next) => {
                    console.log("Get /timeline called");
                    h.getIdeas()
                        .then(data => {
                            console.log("*****************************************************");
                            console.log(data);
                            console.log("*****************************************************");
                            if (req.session._id == undefined)
                                res.render('login');
                            else if (data[0] !== undefined) {
                                let allDomain = h.setDomains(req, data);                            
                                h.setdm(allDomain)
                                    .then( allDomain =>{
                                        req.session.allDomain = allDomain;
                                        h.getUsers()
                                            .then(udata=>{
                                                console.log("Data",udata);
                                                req.session.allUser = udata;
                                            res.render('timeline', {
                                                uid: req.session.uid,
                                                _id: req.session._id,
                                                host: config.host,
                                                iData: data,
                                                iDomain: (req.session.allDomain)
                                            });
                                        })
                                        .catch(err=>{
                                            console.log("Error While fetching users Data",err);
                                    });
                                })
                                .catch(err=>{
                                    console.log(err);
                                });
                                return;
                            } else {
                                res.render('timeline', {
                                    uid: req.session.uid,
                                    _id: req.session._id,
                                    host: config.host,
                                    iData: {},
                                    iDomain: {}
                                });
                            }

                        })
                        .catch(err=>{
                            console.log(err);
                        });
                },
                '/typography': (req, res, next) => {
                    res.render('typography');
                },
                '/notifications': (req, res, next) => {
                    res.render('notifications');
                },
                '/': (req, res, next) => {
                    console.log(req.session._id == undefined);
                    if (req.session._id !== undefined)
                        res.redirect('timeline');
                    else
                        res.render('login');
                },
                '/login': (req, res, next) => {
                    req.session.destroy();
                    req.logout();
                    req.logout();
                    res.redirect('/');
                }
            },
            'POST': {
                '/insertideaDL': (req, res, next) => {
                    h.instIdea(req.body)
                        .then(data => {
                            console.log(data.insertedIds[1]);
                            res.writeHead(200);
                            res.end(JSON.stringify(data.insertedIds[1]));
                        })
                        .catch(err => {
                            console.log(err);
                            res.writeHead(501);
                            res.end(err);
                        })
                },
                '/auth': (req, res, next) => {
                    h.authenticate(req)
                        .then(data => {
                            if (data == null) {
                                console.log("Error: not Found");
                                res.redirect('/');
                            } else {
                                req.session._id = data._id;
                                req.session.uid = data.uid;
                                res.redirect(301, '/timeline');
                            }
                        })
                        .catch(error => console.error(error));
                }
            },
            'NA': (req, res, next) => {
                res.status(404);
                res.render("blankTemp");
            }
        }
        //iteration thru all objects
    return h.route(routes);
}