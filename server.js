'use strict';
const Express = require('express');
const app = Express();
const bodyParser = require('body-parser');
const Platonidea = require('./app');
const passport = require('passport');
app.set('port',process.env.PORT || 22089);
app.use(Express.static('public'));
app.set('view engine','ejs');
app.set('views','pages');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(Platonidea.session);
app.use(passport.initialize());
app.use(passport.session());
app.use('/',Platonidea.router) ;
app.use(require('morgan')('combined',{
    stream:{
        write:message=>{
            
        }
    }
}));
/*Platonidea.ioServer(app).listen(app.get('port'),()=>{
    console.log("Server is running...");
});*/
Platonidea.ioServer(app).listen(22089,()=>{
    console.log("Server is running...");
});